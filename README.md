 # pytm



## Getting started
- [ ] Must have access to a linux terminal to perform manual steps.
- [ ] Must have git installed on your linux terminal.
- [ ] Must have python3 and pip3 is installed on your linux terminal. Many of the DevSecOps tools use this for installation.
- [ ] If the tool has CI/CD integration below, you must have access to a gitlab server and runner to deploy the CI/CD config file (.gitlab-ci.yml)

## Tool Description
Traditional threat modeling too often comes late to the party, or sometimes not at all. In addition, creating manual data flows and reports can be extremely time-consuming. The goal of pytm is to shift threat modeling to the left, making threat modeling more automated and developer-centric.

Based on your input and definition of the architectural design, pytm can automatically generate the following items:
- Data Flow Diagram (DFD)
- Sequence Diagram
- Relevant threats to your system

[pytm github](https://github.com/izar/pytm)

## Manual Install
Steps performed on Ubuntu 22.04.2 LTS. Adjust installation commands based on your distro.

To generate diagrams, and reports, pytm uses:
- dot from graphviz
- pandoc
- plantuml

plantuml is used to generate sequence diagrams, so we will consider it an optional dependency for now and proceed with installing just graphviz and pandoc.
```
apt update && apt install graphviz pandoc -y
git clone https://github.com/izar/pytm.git && cd pytm
pip3 install -r requirements.txt
python3 ./tm.py --help
cat -n tm.py
```
Examine the yaml file. Head over to the official pytm repo for further information on how to further configure.

Now we will generate an example data flow diagram, json, as well as a html report
```
python3 ./tm.py --dfd | dot -Tpng -o sample.png
python3 ./tm.py --json sample.json
python3 ./tm.py --report docs/basic_template.md | pandoc -f markdown -t html > sample.html
python3 -m http.server 8000`
```
You can now view the .png file and html report that was generated by heading to localhost or IP address of your linux terminal over port 8000 using a web browser. Alternatively, all example files can be viewed in the gitlab pytm repository.

![PNG file](sample.png)

## CI/CD Integration
N/A

